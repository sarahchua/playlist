export type AmplifyDependentResourcesAttributes = {
  "api": {
    "testamplifyfrontend": {
      "GraphQLAPIEndpointOutput": "string",
      "GraphQLAPIIdOutput": "string",
      "GraphQLAPIKeyOutput": "string"
    }
  }
}