import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import { Amplify } from '@aws-amplify/core';
import awsmobile from './aws-exports';

Amplify.configure(awsmobile);

createApp(App).mount('#app')
